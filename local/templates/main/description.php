<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

$arTemplate = array(
	"NAME" => Loc::getMessage("TEMPLATES_MAIN_DESCRIPTION_NAME"),
	"DESCRIPTION" => "",
	"SORT" => "500",
	"EDITOR_STYLES" => array(
		"/local/templates/main/assets/css/bootstrap.min.css",
		"/local/templates/main/assets/css/font-awesome.min.css",
		"/local/templates/main/assets/css/style.css",
	)
);
?>