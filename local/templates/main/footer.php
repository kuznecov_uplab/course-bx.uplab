</div> <!-- .col-md-8 -->

<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
	"AREA_FILE_SHOW" => "sect",
	"AREA_FILE_SUFFIX" => "aside",
	"AREA_FILE_RECURSIVE" => "Y",
));?>

</div> <!-- /.row -->
</div> <!-- /.container -->


<footer class="container-fluid footer">
	<div class="container text-center">
		<div class="footer-logo"><img src="/img/light-bike.png" alt=""></div>
		<p class="laread-motto"><?=$APPLICATION->ShowProperty("sub-title")?></p>
		<div class="laread-social">
			<a href="#" class="fa fa-twitter"></a>
			<a href="#" class="fa fa-facebook"></a>
			<a href="#" class="fa fa-pinterest"></a>
		</div>
	</div>
</footer>


<!-- </div> /.canvas -->
</body>
</html>
