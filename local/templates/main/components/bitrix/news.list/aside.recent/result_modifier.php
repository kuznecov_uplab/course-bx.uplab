<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

d($arResult, "result_modifier");


$names = array();
foreach ($arResult["ITEMS"] as $item) {
	$names[] = $item["NAME"];
}


$arResult["NAMES"] = $names;


$this->__component->setResultCacheKeys(array(

	"NAMES", "XML_ID"

));