<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<ul class="laread-list">
	<li class="title">ПОСЛЕДНИЕ ЗАПИСИ</li>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<li>
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
		<i class="date">
			<?=$arItem["DISPLAY_ACTIVE_FROM"]?>
		</i>
	</li>
	<br>
<?endforeach;?>
</ul>