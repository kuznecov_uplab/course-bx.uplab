<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


// ==== получение разделов ===

if(!Bitrix\Main\Loader::includeModule("iblock"))return;
$arSections = [];
$sectsID = [];
foreach ($arResult["ITEMS"] as &$arItem) {
	$sect = $arItem["IBLOCK_SECTION_ID"];
	if(empty($sect)) continue;
	$sectsID[] = $sect;
	$arItem["SECTION"] = &$arSections[$sect];
}
$sectsID = array_unique($sectsID);
if(empty($sectsID)) return;
$sectFilter = array("ID" => $sectsID);
$secOtrder = array("left_margin" => "asc");
$rsSect = CIBlockSection::GetList($secOtrder, $sectFilter);
while ($arSect = $rsSect->GetNext()) {
	$arSections[$arSect["ID"]] = $arSect;
}

// \\\\ получение разделов ////


foreach ($arResult["ITEMS"] as &$arItem) {

	$arItem["PICTURE"] = CFile::ResizeImageGet(
		$arItem["PREVIEW_PICTURE"],
		array("width"=>800, "height"=>600),
		// BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		BX_RESIZE_IMAGE_EXACT,
		false
	)['src'];

	$arItem["PICTURES"] = [];

	if(!empty($pictures = $arItem["PROPERTIES"]["PICTURES"]["VALUE"])) {
		foreach ($pictures as $pic) {
			$arItem["PICTURES"][] = CFile::ResizeImageGet(
				$pic,
				array("width"=>800, "height"=>600),
				// BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				BX_RESIZE_IMAGE_EXACT,
				false
			)['src'];
		}
	}

}


d($arResult["ITEMS"]);