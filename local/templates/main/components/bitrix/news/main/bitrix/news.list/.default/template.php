<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="post-fluid post-medium-vertical">

<?foreach($arResult["ITEMS"] as $arItem):?>
<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>

	<div class="container-fluid post-default">
		<div class="container-medium">
			<div class="row post-items" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

<?if(!empty($arItem["PICTURES"])):?>


				<div class="post-item-banner lg-banner">
					<div id="carousel-example-generic_<?=$arItem["ID"]?>" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
<?	foreach ($arItem["PICTURES"] as $key => $value):?>
							<li data-target="#carousel-example-generic_<?=$arItem["ID"]?>" data-slide-to="<?=$key?>" class="active"></li>
<?	endforeach;?>
						</ol>
						<div class="carousel-inner">
<?	$f = true; foreach ($arItem["PICTURES"] as $key => $pic):?>
							<div class="item <?=$f?"active":""?>">
								<img src="<?=$pic?>" alt="" />
							</div>
<?	$f = false; endforeach;?>
						</div>
						<a class="left carousel-control" href="#carousel-example-generic_<?=$arItem["ID"]?>" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="right carousel-control" href="#carousel-example-generic_<?=$arItem["ID"]?>" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>

<?else:?>

				<div class="post-item-banner">
					<img src="<?=$arItem["PICTURE"]?>" alt="<?=$arItem["NAME"]?>" />
				</div>

<?endif;?>

				<div class="col-md-12">
					<div class="post-item">
						<div class="post-item-paragraph">
							<div>
								<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
								<a href="<?=$arItem["SECTION"]["SECTION_PAGE_URL"]?>" class="mute-text"><?=$arItem["SECTION"]["NAME"]?></a>
							</div>
							<h3>
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
									<?=$arItem["NAME"]?>
								</a>
							</h3>
							<div class="p">
								<?=$arItem["PREVIEW_TEXT"]?>
								<a href="#" class="more">[...]</a>
							</div>
						</div>
						<div class="post-item-info clearfix">
							<div class="pull-left">
								<span><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
<?/*
								•
								<a href="#">Daniele Zedda</a>
*/?>
							</div>
<?/*
							<div class="pull-right post-item-social">
								<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
								<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
								<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
							</div>
*/?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?endforeach;?>

</div><?return;?>

<div class="container-fluid post-default">
	<div class="container-medium">
		<div class="row post-items">
			<div class="post-item-banner">
				<img src="/img/banner-40.jpg" alt="" />
			</div>
			<div class="col-md-12">
				<div class="post-item">
					<div class="post-item-paragraph">
						<div>
							<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
							<a href="#" class="mute-text">DESIGN</a>
						</div>
						<h3><a href="#">Meet #59 Interface Designer John Doe</a></h3>
						<p>Praesent mollis sodales est, eget fringilla libero sagittis eget. Nunc gravida varius risus ac luctus. Mauris ornare eros sed libero euismod ornare. Nulla id sem a mauris egestas pulvinar vitae non dui. Cras odio tortor, feugiat nec sagittis sed, laoreet ut mauris. In hac habitasse platea dictumst. Mauris non libero ligula, sed volutpat mauris <a href="#" class="more">[...]</a></p>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>28 June</span>   •   By <a href="#">Daniele Zedda</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid post-single">
	<div class="container-medium">
		<div class="row post-items">
			<div class="col-md-12">
				<div class="post-item">
					<div class="post-item-paragraph">
						<div>
							<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
							<a href="#" class="mute-text">26 June 2015</a>
						</div>
						<h3><a href="#">Workshop: Brand Asset Management</a></h3>
						<p class="five-lines">Consectetur adipiscing elit. Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit. Nunc sed dolor enim, vitae sodales diam. Aenean imperdiet urna a lectus imperdiet consequat. Fusce eu nibh metus. Curabitur nec dignissim diam. Nulla eget massa at urna sagittis malesuada eget a erat. Sed vel magna leo, in pretium nunc. Ut ornare turpis vel ipsum vulputate lacinia. Pellentesque blandit sagittis tempor. Curabitur adipiscing est vitae quam bibendum at euismod ligula dignissim. Duis nec volutpat leo. Nam mollis massa ut nibh blandit ac faucibus metus tincidunt. Cras sagittis facilisis dui, id posuere tortor aliquam in. Aenean rhoncus purus a tortor posuere at interdum mi venenatis. Integer at urna quis nulla egestas dapibus. <a href="#">[...]</a></p>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							By <a href="#">Jason Bourne</a>   •   <a href="#">#travel</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid post-banner">
	<div class="container-medium">
		<div class="row post-items">
			<div class="post-item-banner lg-banner">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<img src="/img/banner-41.jpg" alt="" />
						</div>
						<div class="item">
							<img src="/img/banner-42.jpg" alt="" />
						</div>
						<div class="item">
							<img src="/img/banner-43.jpg" alt="" />
						</div>
					</div>
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
				</div>
			</div>
			<div class="col-md-12">
				<div class="post-item">
					<div class="post-item-paragraph">
						<div>
							<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
							<a href="#" class="mute-text">FUN</a>
						</div>
						<h3><a href="#">Harbiye Open Air Concerts</a></h3>
						<p class="ellipsis-fade-five">Vodafone Smart 6 Harbiye Open Air Concerts will be at Harbiye Cemil Topuzlu Open Air Stage. Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit. Nunc sed dolor enim, vitae sodales diam. Mauris fermentum fringilla lorem, in rutrum massa sodales et. Praesent mollis sodales est, lorem eget fringilla libero sagittis animous ledra sitemah eget. Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit. Nunc sed dolor enim, vitae sodales diam. <a href="#" class="more"></a></p>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>25 June</span>   •   By <a href="#">Gannon Burget</a>   •   <a href="#">#concert</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid post-video">
	<div class="container-medium">
		<div class="row post-items">
			<div class="post-item-banner embed-responsive embed-responsive-16by9">
				<iframe src="https://player.vimeo.com/video/49445992" class="embed-responsive-item" allowfullscreen></iframe>
			</div>
			<div class="col-md-12">
				<div class="post-item">
					<div class="post-item-paragraph">
						<div>
							<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
							<a href="#" class="mute-text">Vimeo</a>
						</div>
						<h3><a href="#">Long Live The Kings - Short Film</a></h3>
						<p>Sed vel magna leo, in pretium nunc. Ut ornare turpis vel ipsum vulputate lacinia. Pellentesque blandit sagittis tempor. Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit. Nunc sed dolor enim, vitae sodales diam. Mauris fermentum fringilla lorem, in rutrum massa. Nunc sed dolor enim, vitae sodales diam. Mauris fermentum fringilla lorem, in rutrum massa sodales et. Praesent mollis sodales est, eget fringilla libero sagittis eget. <a href="#" class="more">[...]</a></p>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>24 June</span>   •   <a href="#">#video</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid post-sound">
	<div class="container-medium">
		<div class="row post-items">
			<div class="post-item-banner embed-responsive embed-responsive-16by9">
				<iframe  class="embed-responsive-item" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/186983384&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
			</div>
			<div class="col-md-12">
				<div class="post-item">
					<div class="post-item-paragraph">
						<div>
							<a href="#" class="quick-read qr-only-phone"><i class="fa fa-eye"></i></a>
							<a href="#" class="mute-text">Soundcloud</a>
						</div>
						<h3><a href="#">Show Me Love (Out Now)</a></h3>
						<p class="five-lines">Consectetur adipiscing elit. Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit. Nunc sed dolor enim, vitae sodales diam. Mauris fermentum fringilla lorem, in rutrum massa sodales et. Praesent mollis sodales est, eget fringilla libero sagittis eget. Nunc gravida varius risus ac luctus. Mauris ornare eros sed libero euismod ornare.  Praesent mollis sodales est, eget fringilla libero sagittis eget. Nunc gravida varius risus ac luctus. Mauris ornare eros sed libero euismod ornare. Praesent mollis sodales est, eget fringilla libero sagittis eget. Nunc gravida varius risus ac luctus. Mauris ornare eros sed libero euismod ornare. Praesent mollis sodales est, eget fringilla libero sagittis eget. Nunc gravida varius risus ac luctus. Mauris ornare eros sed libero euismod ornare. </p>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>24 June</span>   •   <a href="#">#music</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" class="quick-read qr-not-phone"><i class="fa fa-eye"></i></a>
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid post-quote">
	<div class="container-medium">
		<div class="row post-items">
			<div class="col-md-12">
				<div class="post-item">
					<div class="in-quote">
						<img src="/img/banner-99.jpg" alt="">
						<div class="block-overlay">
							<div class="overlay-quote">
								<span class="quote-icon">“</span>
								<a href="#" class="spot">The difference between a successful person and others is not a lack of strength, not a lack of knowledge, but rather a lack of will.</a>
								<span class="name">- Vince Lombardi</span>
							</div>
						</div>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>22 June</span>   •   <a href="#">#vision</a>
						</div>
						<div class="pull-right post-item-social">
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid post-link">
	<div class="container-medium">
		<div class="row post-items">
			<div class="col-md-12">
				<div class="post-item">
					<div class="in-link">
						<img src="/img/banner-100.jpg" alt="" />
						<div class="block-overlay">
							<div class="overlay-link">
								<i class="fa fa-link fa-rotate-90"></i>
								<a href="#">http://themeforest.net/category/wordpress</a>
								<span class="link-text">Inside - Personal or Corparete<br />Flexible and Multipurpose WP Theme</span>
							</div>
						</div>
					</div>
					<div class="post-item-info clearfix">
						<div class="pull-left">
							<span>21 June</span>   •   <a href="#">#project</a>
						</div>
						<div class="post-item-social">
							<a href="#" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<a href='#'><i class='fa fa-facebook'></i></a><a href='#'><i class='fa fa-twitter'></i></a>" class="pis-share"><i class="fa fa-share-alt"></i></a>
							<a href="#" class="post-like"><i class="fa fa-heart"></i><span>28</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



</div>

<?=$arResult["NAV_STRING"]?>