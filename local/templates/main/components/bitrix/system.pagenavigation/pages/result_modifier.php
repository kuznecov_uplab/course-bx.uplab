<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$prevKey = "prev";
$nextKey = "next";

$arItems = array();

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

if($arResult["bDescPageNumbering"] === true)
{

    if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
    {
        if($arResult["bSavePage"])
        {
            $arItems[$prevKey] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]+1);
            $arItems[1] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]+1);
        }
        else
        {
            if (($arResult["NavPageNomer"]+1) == $arResult["NavPageCount"])
                $arItems[$prevKey] = $arResult["sUrlPath"].$strNavQueryStringFull;
            else
                $arItems[$prevKey] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]+1);

            $arItems[1] = $arResult["sUrlPath"].$strNavQueryStringFull;
        }
    }
    else
    {
        $arItems[$prevKey] = "";
        $arItems[1] = "";
    }

    $arResult["nStartPage"]--;
    while($arResult["nStartPage"] >= $arResult["nEndPage"]+1)
    {
        $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

        if ($arResult["nStartPage"] == $arResult["NavPageNomer"])
            $arItems[$NavRecordGroupPrint] = "";

        else
            $arItems[$NavRecordGroupPrint] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"];

        $arResult["nStartPage"]--;
    }

    if ($arResult["NavPageNomer"] > 1)
    {

        if($arResult["NavPageCount"] > 1)
            $arItems[$arResult["NavPageCount"]] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=1";

        $arItems[$nextKey] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]-1);

    }
    else
    {

        if($arResult["NavPageCount"] > 1)
            $arItems[$arResult["NavPageCount"]] = "";

        $arItems[$nextKey] = "";

    }

}
else
{

    if ($arResult["NavPageNomer"] > 1)
    {
        if($arResult["bSavePage"])
        {
            $arItems[$prevKey] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]-1);
            $arItems[1] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=1";
        }
        else
        {

            if ($arResult["NavPageNomer"] > 2)
            {
                $arItems[$prevKey] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]-1);
            }
            else
            {
                $arItems[$prevKey] = $arResult["sUrlPath"].$strNavQueryStringFull;
            }
            $arItems[1] = $arResult["sUrlPath"].$strNavQueryStringFull;

        }
    }
    else
    {
            $arItems[$prevKey] = "";
            $arItems[1] = "";
    }

    $arResult["nStartPage"]++;

    while($arResult["nStartPage"] <= $arResult["nEndPage"]-1)
    {
        if ($arResult["nStartPage"] == $arResult["NavPageNomer"])
            $arItems[$arResult["nStartPage"]] = "";

        else
            $arItems[$arResult["nStartPage"]] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"];

        $arResult["nStartPage"]++;
    }

    if($arResult["NavPageNomer"] < $arResult["NavPageCount"])
    {
        if($arResult["NavPageCount"] > 1)
            $arItems[$arResult["NavPageCount"]] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".$arResult["NavPageCount"];

        $arItems[$nextKey] = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]+1);
    }
    else
    {
        if($arResult["NavPageCount"] > 1)
            $arItems[$arResult["NavPageCount"]] = "";

        $arItems[$nextKey] = "";
    }

}

// if ($arResult["bShowAll"])
// {
//     if ($arResult["NavShowAll"])
//         $arItems["pages"] = $arResult["sUrlPath"]."?".$strNavQueryString."SHOWALL_".$arResult["NavNum"]."=0";
//     else
//         $arItems["all"] = $arResult["sUrlPath"]."?".$strNavQueryString."SHOWALL_".$arResult["NavNum"]."=1";
// }

$arResult["ITEMS"] = $arItems;

// d($arItems);