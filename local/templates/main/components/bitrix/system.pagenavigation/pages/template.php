<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(count($arResult["ITEMS"])<4)return;
?>
<div class="pagination-page 11">
<ul class="pagination-page__list">
<?$prevKey=0;?>

<?foreach($arResult["ITEMS"] as $key => $link):?>
<?	if(intval($key)):?>

<?		if($prevKey && $prevKey+1<$key):?>
	<li class="pagination-page__item pagination-page__item_dots">
		<span class="pagination-page__main">...</span>
	</li>
<?		endif;?>

<?		if($link):?>
	<li class="pagination-page__item">
		<a class="pagination-page__main" href="<?=$link?>"><?=$key?></a>
	</li>
<?		else:?>
	<li class="pagination-page__item pagination-page__item_active">
		<span class="pagination-page__main"><?=$key?></span>
	</li>
<?		endif;?>

<?$prevKey=$key;?>

<?	else:?>

<?		if($link):?>
	<li class="pagination-page__item pagination-page__item_<?=$key?>"">
		<a class="pagination-page__main" href="<?=$link?>"><?=$key?></a>
	</li>
<?		else:?>
	<li class="pagination-page__item pagination-page__item_<?=$key?>"">
		<span class="pagination-page__main"><?=$key?></span>
	</li>
<?		endif;?>

<?	endif;?>
<?endforeach;?>
</ul>
</div>