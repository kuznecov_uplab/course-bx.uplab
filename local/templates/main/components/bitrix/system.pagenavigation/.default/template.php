<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(count($arResult["ITEMS"])<4)return;
?>

<div class="row pages">

    <div class="col-md-6">
<?if(!empty($link = $arResult["ITEMS"]["prev"])):?>
        <a href="<?=$link?>" class="post-nav">
            <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
            Более новые
        </a>
<?endif;?>
    </div>

    <div class="col-md-6 post-right">
<?if(!empty($link = $arResult["ITEMS"]["next"])):?>
        <a href="<?=$link?>" class="post-nav">
            Более старые
            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
        </a>
<?endif;?>
    </div>

</div>
