<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?


$arFilter = array(
	"IBLOCK_ID"=>ARTICLES_IBLOCK
);

$res = CIBlockElement::GetList(
	array("active_from "=>"desc"),
	$arFilter,
	array("DATE_ACTIVE_FROM"));

$arYears = array();

while($arItem = $res->Fetch())
{
	$year = date("Y", strtotime($arItem["DATE_ACTIVE_FROM"]));
	$arYears[$year] = $year;
	// echo $ar_fields["DATE_ACTIVE_FROM"].": ".$ar_fields["CNT"]."<br>";
}


echo "<pre>";
print_r($arYears);
echo "</pre>";




$property_enums = CIBlockPropertyEnum::GetList(
	array("DEF"=>"DESC", "SORT"=>"ASC"),
	array("IBLOCK_ID"=>ARTICLES_IBLOCK, "CODE"=>"SIMILAR_TYPE")
);
$enum = array();
while($enum_fields = $property_enums->GetNext()) {
	$enum[] = $enum_fields;
}


// echo "<pre style='display:none;'>";
// print_r($enum);
// echo "</pre>";

?>

<div class="row row_filter">
<div class="row">

	<div class="col col-md-4"></div>

	<div class="col col-md-5">
		<select class="js-filter-select filter-select" name="SIMILAR_TYPE">
<?foreach ($enum as $item):?>
			<option value="<?=$item["ID"]?>"><?=$item["VALUE"]?></option>
<?endforeach;?>
		</select>
	</div>

	<div class="col col-md-3">
		<select class="js-filter-select filter-select" name="YEAR">
<?foreach ($arYears as $year):?>
			<option value="<?=$year?>"><?=$year?></option>
<?endforeach;?>
		</select>
	</div>

</div>
</div>