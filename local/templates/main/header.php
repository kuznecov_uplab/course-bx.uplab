<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;

global $APPLICATION;
$curDir = $APPLICATION->GetCurDir();

?><!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="/img/favicon.ico">
<title><?$APPLICATION->ShowTitle()?></title>
<?

Asset::getInstance()->addCss("http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet");
Asset::getInstance()->addCss("http://fonts.googleapis.com/css?family=Roboto+Slab&amp;subset=latin,latin-ext");

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/bootstrap.min.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/font-awesome.min.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/jasny-bootstrap.min.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/animate.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/tomorrow-night.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/bootstrap-gallery.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/colorbox.css");

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/select2/dist/css/select2.min.css");

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/style.css");


Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/bootstrap.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jasny-bootstrap.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/prettify.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/lang-css.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.blueimp-gallery.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/imagesloaded.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/masonry.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/viewportchecker.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.dotdotdot.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.colorbox-min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.nicescroll.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/isotope.pkgd.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.ellipsis.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/calendar.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.touchSwipe.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/select2/dist/js/select2.min.js");

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/script.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/prog.js");

$APPLICATION->ShowHead();

?>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script data-skip-moving="true" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script data-skip-moving="true" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>

<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<!-- <div class="page-loader">
	<div class="loader-in">Loading...</div>
	<div class="loader-out">Loading...</div>
</div> -->

<!-- <div class="canvas"> -->
	<div class="canvas-overlay"></div>

	<header>
		<nav class="navbar navbar-fixed-top navbar-laread">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?=SITE_DIR?>">
						<img height="64" src="/img/logo-bike.png" alt="">
					</a>
				</div>
				<div class="collapse navbar-collapse" id="main-nav">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top",
	array(
		"COMPONENT_TEMPLATE" => "horizontal_multilevel",
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
				</div><!-- /.nav-collapse -->
			</div>
		</nav>
	</header>

	<div class="container">
		<div class="head-text">
			<h1><?$APPLICATION->ShowTitle(false)?></h1>
			<p class="lead-text"><?$APPLICATION->ShowProperty("sub-title")?></p>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">