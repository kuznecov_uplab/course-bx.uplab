$(function(){
	$('select').select2();
});

$(function(){
	var SELECT_SEL = '.js-filter-select';
	var CONTENT_SET = '.js-content';

	$(SELECT_SEL).on('change', function(){
		var $this = $(this);
		// console.log($this.val());

		var data = {
			ajax: 'y',
		};
		data[$this.attr('name')] = $this.val();

		$(CONTENT_SET).load(
			location.href,
			data
		);

	});

})