<?
define('LOG_FILENAME', __DIR__.'/log/'.date('Ymd').'.log');

# автозагрузка классов из папки classes
include __DIR__ . "/classes/autoload.php";

# события
include __DIR__ . "/include/events.php";

# глобальные функции
if(defined('UP_ENV_TYPE')) {
	include __DIR__ . "/include/functions_" . UP_ENV_TYPE . ".php";
} else {
	include __DIR__ . "/include/functions.php";
}

# автогенерация констант инфоблоков
Uplab\Constant::define();