<?
namespace Uplab;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;
use CIBlockSection;
use CIBlockElement;
use CFile;
use CHTTP;
use CTimeZone;
use CLang;
use Cutil;
use ZipArchive;

/**
 * universal helping class for Bitrix projects
 */
class Helper
{
	protected static $instance = null;

	function __construct() {
		$this->app = $GLOBALS["APPLICATION"];
		$this->user = $GLOBALS["USER"];
	}

	public static function getInstance() {
		if (is_null(self::$instance))
			self::$instance = new static();
		return self::$instance;
	}

	public static function getDocRoot(){
		static $docRoot = false;
		if(!$docRoot){
			if(!($docRoot=$_SERVER["DOCUMENT_ROOT"]))
				$docRoot = realpath(__DIR__."/../../../../");
		}
		return $docRoot;
	}

	public static function getPhpIntDir(){
		static $phpIntDir = false;
		if(!$phpIntDir){
			$phpIntDir = realpath(__DIR__."/../../");
		}
		return $phpIntDir;
	}

	public static function getJsLibs(){
		static $jsLibs = false;
		if(!$jsLibs){
			$jsLibs = include self::getPhpIntDir() . "/include/jslibs.php";
		}
		return $jsLibs;
	}

	public static function getCurPage(){
		if(!isset(self::getInstance()->curPage)){
			self::$instance->curPage = self::$instance->app->GetCurDir();
		}
		return self::getInstance()->curPage;
	}

	public static function set404(){
		@define('ERROR_404','Y');
		$GLOBALS["is404"] = 1;
		CHTTP::SetStatus("404 Not Found");
	}

	public static function showBxHead(){
		self::getInstance()->app->ShowHead();
		return;
		if(self::getInstance()->user->isAdmin()){

		}else{
			echo "<meta charset=\"".LANG_CHARSET."\">".PHP_EOL;
			self::getInstance()->app->ShowMeta("robots");
			self::getInstance()->app->ShowMeta("keywords");
			self::getInstance()->app->ShowMeta("description");
			self::getInstance()->app->ShowLink("canonical");
			self::getInstance()->app->ShowCSS();
		}
	}

	public static function showBxFoot(){
		return;
		if(!self::getInstance()->user->isAdmin()){
			self::getInstance()->app->ShowHeadStrings();
			self::getInstance()->app->ShowHeadScripts();
		}
	}

	public static function getTemplateResource($src){
		return Cutil::GetAdditiONALfILeURL(
			self::getInstance()->app->getTemplatePath($src)
		);
	}

	public static function getBxTime($time = false, $format = 'FULL', $offset = false){
		if(!$time)  $time=time();
		if($offset) $time+=CTimeZone::GetOffset();
		return ConvertTimeStamp($time,$format);
	}

	public static function getFilterByDate($param){
		$year_1  = date('Y');
		$year_2  = $year_from + 1;
		$month_1 = 1;
		$month_2 = 1;
		$day_1   = 1;
		$day_2   = 1;
		extract($param);

		return array(
			date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,$month_1,$day_1,$year_1)),
			date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,$month_2,$day_2,$year_2))
		);
	}

	public static function getFilterByYear($year=false){
		if(!$year)
			$year = date('Y');

		return array(
			date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,1,1,$year)),
			date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,1,1,$year+1))
		);
	}

	public static function getBxDateFormat($format="SHORT"){
		if(!self::getInstance()->dateFormat[$format])
			self::$instance->dateFormat = $GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat($format));
		return self::getInstance()->dateFormat[$format];
	}

	public static function getRequestValues($keys){
		$arRequest = array();
		foreach ((array)$keys as $key) {
			if(isset($_REQUEST[$key]))
				$arRequest[$key] = htmlspecialchars($_REQUEST[$key]);
		}
		return $arRequest;
	}

	public static function translit($name, $dash=false, $lng=false, $length=false){
		if($dash===false)$dash="_";
		if(!$lang)$lang="ru";

		$arParams = array(
			"replace_space" => $dash,
			"replace_other" => $dash
		);

		if(intval($length))
			$name = trim(substr($name, 0, $length));

		return Cutil::translit($name, $lng, $arParams);
		// str_replace('/', '_', subject)
		// preg_replace('~~', replacement, subject)
	}

	public static function clearPhone($phone){
		$phone = trim($phone);
		return $phone[0].self::translit(substr($phone,1),"");
	}

	public static function Log($arr, $path='/local/log.txt'){
		file_put_contents($_SERVER['DOCUMENT_ROOT'].$path, print_r($arr,true));
	}

	/**
	 * Рекурсивно искать файл шаблона страницы в папке шаблона сайта
	 * Первый путь для поиска /include_pages/LN/ (LN - текущий язык)
	 * Общие для обоих языков шаблоны в корне папок /inc_pages, /inc_areas
	 */
	public static function includeFile($file,$dir=false,$params=array(),$options=array(),$recursive=true){
		if(!preg_match('~\.php$~',$file))
			$file = $file.'.php';
		if($dir===false)
			$dir = self::getCurPage();
		if($recursive)
			$file = $GLOBALS['APPLICATION']->GetFileRecursive($file, $dir);
		else
			$file = $dir.$file;
		if(!empty($file)){
			return $GLOBALS['APPLICATION']->IncludeFile($file,$params,$options);
		}
	}
	/**
	 * Фуннкция для рекурсивного подключения страницы
	 */
	public static function includePage($file,$params=array(),$options=array("SHOW_BORDER"=>false)){
		return self::includeFile(
			$file,
			SITE_TEMPLATE_PATH.'/inc_pages'.SITE_DIR,
			$params,
			$options
		);
	}
	/**
	 * Фуннкция для рекурсивного подключения области
	 * В отличие от остальных методов, у includeArea отличается порядок параметорв
	 * Второй параметр - массив опций. При необходимости параметры можно предать третьим параметром.
	 */
	public static function includeArea($file,$options=array(),$params=array()){
		return self::includeFile(
			$file,
			SITE_TEMPLATE_PATH.'/inc_areas'.SITE_DIR,
			$params,
			$options
		);
	}
	/**
	 * Фуннкция для рекурсивного подключения элемента шаблона
	 */
	public static function includeView($file,$params=array(),$options=array("SHOW_BORDER"=>false)){
		return self::includeFile(
			$file,
			SITE_TEMPLATE_PATH.'/inc_views'.SITE_DIR,
			$params,
			$options
		);
	}
	public static function includeComponent($component,$template,$params){
		global $APPLICATION;
		include self::getPhpIntDir()."/include/components/".explode(":",$component)[1].".php";
	}

	public static function getPageCode($curPage=""){
		if(!self::getInstance()->pageCode){
			if(empty($curPage))
				$curPage = self::getInstance()->app->GetCurDir();
			preg_match('~(/[a-zA-Z0-9-_/]+)*/([a-zA-Z0-9_]+)/.*~', $curPage, $matches);
			self::$instance->pageCode = $matches[2];
		}
		return self::getInstance()->pageCode;
	}

	public static function getLevelUpLink($curPage=false){
		$url = explode("/", self::getCurPage());
		array_pop($url);
		array_pop($url);
		$url = implode("/", $url)."/";
		return $url;
	}

	public function getMetaDate($date){
		return date('Y-m-d',strtotime($date));
	}

	public function getFileInfo($fileID){
		$fileItem = CFile::GetFileArray($fileID);
		$fileItem['SIZE'] = CFile::FormatSize($fileItem['FILE_SIZE']);
		$fileItem['EXT'] = strtoupper( GetFileExtension($fileItem['SRC']) );
		return $fileItem;
	}

	public static function loadMainMsg(){
		Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/main.php');
	}

	/**
	 * Получение склонения слова после числа
	 * @param int $number число
	 * @return int
	 */
	public static function getMultipleWord($number,$msg){
		self::loadMainMsg();
		$num = ($number > 20) ? $number % 10 : $number;
		if ($num >= 5 || $num == 0) $id=3;
		elseif ($num >= 2) $id=2;
		elseif ($num == 1) $id=1;
		if(!$word = Loc::getMessage($msg.'_'.$id,['#NUM#'=>$number]))
			$word = Loc::getMessage($msg,['#NUM#'=>$number]);
		return $word;
	}

	public static function addJs($array = array()){
		if(!is_array($array)) $array=array($array);
		$jsLibs = self::getInstance()->getJsLibs();
		foreach ($array as $val) {
			if($arSrc=$jsLibs[$val]) {

				if(!is_array($arSrc)) $arSrc=array($arSrc);
				foreach ($arSrc as $src){
					if(stripos($src, '//')!==false){
						Asset::getInstance()->addJs($src);
					} else {
						Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.$src);
					}
				}

			} else {
				Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/{$val}.js");
			}
		}
	}

	public static function cutString($text,$len=false)
	{
		if(!$len) $len = 40;
		$parser = new \CTextParser;
		if($len > 0)
			$text = $parser->html_cut($text, $len);
		return $text;
	}

	public static function resizeImage($params){
		$width = 200;
		$height = 200;
		$get_size = false;
		$get_meta = false;
		$get_default = false;
		$method = BX_RESIZE_IMAGE_PROPORTIONAL_ALT;
		extract($params);
		foreach ((array)$picture as $key=>$pic){
			if(empty($pic))continue;
			$srcPic = $pic;
			break;
		}

		$size = array("width"=>$width, "height"=>$height);

		if(empty($srcPic)) {

			if(!$get_default) return false;

			$src = self::getDocRoot()."/upload/default_picture_{$width}_{$height}_{$method}.jpg";

			if(!file_exists($src)) {
				CFile::ResizeImageFile(
					self::getDocRoot().DEFAULT_PICTURE,
					$src,
					$size,
					$method
				);
			}

			$src = CUtil::GetAdditionalFileURL(str_replace(self::getDocRoot(), "", $src));
			$newPic = compact("src","width","height");

		} else {
			$newPic = CFile::ResizeImageGet($srcPic,$size,$method,$get_size);
		}

		return array_change_key_case($newPic, CASE_UPPER);
	}

}