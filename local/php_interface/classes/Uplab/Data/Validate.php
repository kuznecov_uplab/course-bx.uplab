<?
namespace Uplab\Data;


class Validate
{
	const EMAIL = '~^[^@]+@[^@]+$~';

	public static function check($input, $pattern="EMAIL")
	{
		if($pattern=constant("self::".strtoupper($pattern)))
			return preg_match($pattern, $input);
		return false;
	}

	public static function checkArray($input=[], $pattern="EMAIL")
	{
		$valid = [];
		foreach ($input as $item) {
			$item = trim($item);
			if(self::check($item,$pattern)) $valid[] = $item;
		}
		return $valid;
	}

}