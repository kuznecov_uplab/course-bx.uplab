<?
namespace Uplab\Iblock;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;
use CIBlockSection;
use CIBlockElement;
use CFile;
use CHTTP;
use CTimeZone;
use CLang;
use Cutil;
use ZipArchive;



class Helper
{

	public static function getList($arParams = array())
	{
		if(!Loader::includeModule("iblock"))return;
		$arResult   = array();
		$arOrder    = array_merge(array(), (array)$arParams['order']);
		// $arSelect   = array_merge(array('ID', 'IBLOCK_ID', 'NAME', 'ACTIVE', 'DATE_ACTIVE_FROM'), $arParams['select']);
		$arSelect   = array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'ACTIVE', 'DATE_ACTIVE_FROM');
		if($arParams['select']) $arSelect = array_merge($arSelect,(array)$arParams['select']);
		$arFilter   = array_merge(array('ACTIVE_DATE'=>'Y', 'ACTIVE'=>'Y'), (array)$arParams['filter']);
		$arNavStart = $arParams['nav_param'];
		if(!$arNavStart) $arNavStart = false;
		$res = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStart, $arSelect);
		if($arParams['props']!='N')
		{
			while($ob = $res->GetNextElement())
			{
				$el = $ob->GetFields();
				$el['PROPERTIES'] = $ob->GetProperties();

				if(isset($arParams['by'])){
					if($arParams['by']=='N')
						$arResult[] = $el;
					elseif($key=$el[$arParams['by']])
						$arResult[$key] = $el;
					else
						$arResult[$el['ID']] = $el;
				} else {
					$arResult[$el['ID']] = $el;
				}
			}
		}
		else
		{
			while($el = $res->GetNext())
			{
				if(isset($arParams['by'])){
					if($arParams['by']=='N')
						$arResult[] = $el;
					elseif($key=$el[$arParams['by']])
						$arResult[$key] = $el;
					else
						$arResult[$el['ID']] = $el;
				} else {
					$arResult[$el['ID']] = $el;
				}
			}
		}
		return $arResult;
	}

	public static function sectGetList($arParams = array())
	{
		if(!Loader::includeModule('iblock'))return;
		$arResult = array();
		$arOrder  = array_merge(array('left_margin' => 'asc'), (array)$arParams['order']);
		$arSelect = array_merge(array(), (array)$arParams['select']);
		$arFilter = array_merge(array('GLOBAL_ACTIVE'=>'Y'), (array)$arParams['filter']);
		$count = $arParams['count']===true ? ture : false;
		$res = CIBlockSection::GetList($arOrder, $arFilter, $count, $arSelect);

		while ($sect = $res->GetNext())
		{
			if(isset($arParams['by']) && $key=$sect[$arParams['by']])
				$arResult[$key] = $sect;
			else
				$arResult[$sect['ID']] = $sect;
		}
		return $arResult;
	}

	public static function sectGetTree($arParams = array())
	{
		if(!Loader::includeModule("iblock"))return;
		$arSelect = array_merge(array('NAME','ID','DEPTH_LEVEL'),(array)$arParams['select']);
		$arFilter = array_merge(array('GLOBAL_ACTIVE'=>'Y'), (array)$arParams['filter']);
		$arOrder  = array_merge(array('left_margin'=>'asc','id'=>'asc'), (array)$arParams['order']);
		$res = CIBlockSection::GetList(array(), $arFilter, true, array());
		if($rootSect = $res->GetNext())
		{
			$arSects = array();
			$arFilter = array_merge(array(
				'IBLOCK_ID'  => $rootSect['IBLOCK_ID'],
				// 'GLOBAL_ACTIVE' => 'Y',
				'>LEFT_MARGIN'  => $rootSect['LEFT_MARGIN'],
				'<RIGHT_MARGIN' => $rootSect['RIGHT_MARGIN'],
				'>DEPTH_LEVEL'  => $rootSect['DEPTH_LEVEL']
			), (array)$arParams['sect_filter']);
			$res = CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
			while($sect = $res->GetNext())
			{
			  $arSects[$sect['ID']] = $sect;
			}
		}
		return array(
			'root' => $rootSect,
			'sects' => $arSects
		);
	}

	public static function getCount($iblock)
	{
		return CIBlockElement::GetList(false, ["IBLOCK_ID" => $iblock], []);
	}

	public static function getSectionsList($param,&$arSections,&$debug)
	{
		if(!Loader::includeModule("iblock"))return;
		extract($param);
		$sectsID = [];
		$count = 0;

		$addURL = !empty($navParams) ? "?".http_build_query($navParams) : "";

		$sectFilter = ["IBLOCK_ID"=>$iblock];

		foreach ($filter as $key => $filterVal) {
			if(strripos($key, "PROPERTY_")===false) continue;
			$key = str_replace("PROPERTY_", "", $key);
			$sectFilter["PROPERTY"][$key] = $filterVal;
		}

		$sectFilter["ID"] = array_keys($sectsID);
		$sectSelect = ["ID","NAME","SECTION_PAGE_URL"];
		$res = CIBlockSection::getList(["left_margin"=>"asc"],$sectFilter,true,$sectSelect);
		$isSelected = false;
		while($sect = $res->getNext()){
			if($sect["CODE"] == $current){
				$sect["SELECTED"] = "Y";
				$isSelected = true;
			}
			$count += $sect["ELEMENT_CNT"];
			$sect["SECTION_PAGE_URL"] .= $addURL;
			$arSections[$sect["ID"]] = $sect;
		}

		if(isset($arSections["all"])) {
			$arSections["all"]["ELEMENT_CNT"] = $count;
			$arSections["all"]["SECTION_PAGE_URL"] .= $addURL;
			if(empty($current))
				$arSections["all"]["SELECTED"] = "Y";
		}

	}

	public static function prepareTags($strTags)
	{
		$arTags = array(
			"SRC" => $strTags
		);
		$arTags["NAMES"] = preg_split("~\s*,\s*~", $strTags);
		$arTags["NAMES"] = array_diff($arTags["NAMES"], [""]);
		foreach ($arTags["NAMES"] as $tag) {
			$arTags["%NAMES"][] = "%{$tag}%";
		}
		$arTags["URL"] = urlencode(implode(",", $arTags["NAMES"]));
		$arTags["URL"] = empty($arTags["URL"]) ? "" : "?tag=" . $arTags["URL"];
		return $arTags;
	}

	public static function similarsByTags($param, &$arTags, &$arItems)
	{
		$curCnt 	= 4;
		$otherCnt 	= 3;
		$curExclude = [PHOTO_IBLOCK, VIDEO_IBLOCK];
		$picW 		= 375;
		$picH 		= 300;
		extract($param);
		$iblocks = (array)$iblocks;
		if(empty($iblocks)) return false;

		$arTags = self::prepareTags($strTags);

		$arFilter = array(
			"TAGS" => $arTags["%NAMES"],
			"!PREVIEW_PICTURE" => false
		);

		if(intval($curID)) $arFilter["!ID"] = $curID;

		$arSelect = array(
			"ID","IBLOCK_ID","NAME",
			"TAGS",
			"PREVIEW_PICTURE",
			"DETAIL_PICTURE",
			"DETAIL_PAGE_URL",
			"PROPERTY_VIDEO_URL"
		);
		$arItems = array();

		foreach ($iblocks as $iblock) {
			$filter = $arFilter;
			$filter["IBLOCK_ID"] = $iblock;

			if(in_array($iblock, $curExclude)){
				$count = $otherCnt;
			} else {
				$count = $iblock == $curIblock ? $curCnt : $otherCnt;
			}

			$res = CIblockElement::GetList([],$filter,false,["nTopCount"=>$count],$arSelect);
			while ($item = $res->getNext()) {
				$item["PICTURE"] = \Uplab\Helper::resizeImage(array(
					"width" => $picW,
					"height" => $picH,
					"method" => BX_RESIZE_IMAGE_EXACT,
					"picture" => [$item["PREVIEW_PICTURE"],$item["DETAIL_PICTURE"]]
				))["SRC"];
				$arItems[$item["IBLOCK_ID"]][] = $item;
			}
		}
	}

	public function prepareElementsWithSections(&$arResult)
	{
		if(!Loader::includeModule("iblock"))return;
		$arSections = [];
		$sectsID = [];

		foreach ($arResult["ITEMS"] as &$arItem) {
			if(empty($sect = $arItem["IBLOCK_SECTION_ID"])) {
				$sect = "root";
			} else {
				$sectsID[] = $sect;
			}
			$arItem["SECTION"] = &$arSections[$sect];
		}

		if(empty($sectsID)) return;

		$sectFilter = array(
			"ID" => $sectsID
			// , "ACTIVE" => "Y"
		);
		$secOtrder = array("left_margin" => "asc");
		$rsSect = CIBlockSection::GetList($secOtrder, $sectFilter);

		while ($arSect = $rsSect->GetNext()) {
			$arSections[$arSect["ID"]] = $arSect;
		}

		$arResult["SECTIONS"] = $arSections;
	}

	public function prepareSectionsWithElements(&$arResult)
	{
		if(!Loader::includeModule("iblock"))return;
		$arSections = [];
		$sectItems = [];
		$sectsID = [];

		foreach ($arResult["ITEMS"] as &$arItem) {

			if(empty($sect = $arItem["IBLOCK_SECTION_ID"])) {
				$sect = "root";
			} else {
				$sectsID[] = $sect;
			}

			// $arItem["SECTION"] = &$arSections[$sect];
			$arItem["SECT_ID"] = $sect;
			$sectItems[$sect][] = &$arItem;
			unset($arItem);
		}

		if(empty($sectsID)) return;

		$sectFilter = array(
			"ID" => $sectsID
			// , "ACTIVE" => "Y"
		);
		$secOtrder = array("left_margin" => "asc");
		$rsSect = CIBlockSection::GetList($secOtrder, $sectFilter);

		while ($arSect = $rsSect->GetNext()) {
			$arSect["ITEMS"] = &$sectItems[$arSect["ID"]];
			$arSections[$arSect["ID"]] = &$arSect;
			unset($arSect);
		}

		$arResult["SECTIONS"] = $arSections;
	}

}