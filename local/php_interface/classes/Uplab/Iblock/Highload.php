<?
namespace Uplab\Iblock;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use CIBlockProperty;
use CDBResult;
use Bitrix\Highloadblock\HighloadBlockTable as HiloadTable;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity;

/**
* Класс для упрощения работы с Highload-блоками
*/
class Highload
{
	protected $tableName = null;
	protected $entityClass = null;
	protected $params = [
		"select" => ["*"],
		"filter" => [],
		"order" => []
	];
	protected $result = null;

	function __construct($tableName=false, $iblock=false, $code=false)
	{
		if($tableName) {
			$this->tableName = $tableName;
		} elseif ($iblock && $code) {
			$this->makeFromProperty($iblock,$code);
		} else {
			return null;
		}

		$this->prepareEntity();

		return $this->entityClass;
	}

	public function entity()
	{
		return $this->entityClass;
	}

	public function getDirectory()
	{
		$entity = $this->entityClass;
		if(!$entity) return false;
		$params = array(
			"order" => array("UF_SORT"=>"asc"),
			"select" => array(
				"ID",
				"NAME" => "UF_NAME",
				"CODE" => "UF_XML_ID",
				"SORT" => "UF_SORT"
			)
		);
		$array = array();
		$array["any"] = array(
			"NAME" => "Все",
			"CODE" => ""
		);
		$res = $entity::getList($params);
		while ($item = $res->fetch()) {
			$array[$item["CODE"]] = $item;
		}
		return $array;
	}

	protected function makeFromProperty($iblock,$code)
	{
		$prop = CIBlockProperty::GetList(
			array(),
			array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock, "CODE"=>$code)
		)->fetch();
		$this->tableName = $prop['USER_TYPE_SETTINGS']['TABLE_NAME'];
		if(!$this->tableName)return;
		$this->prepareEntity();
	}

	public function getIblock()
	{
		if(!Loader::includeModule("highloadblock"))return;

		# получить Хайлоад-блок
		$hblock = HiloadTable::getList(array(
			"select" => array("ID","NAME","TABLE_NAME"),
			"filter" => array("TABLE_NAME"=>$this->tableName)
		))->fetch();
		if(!$hblock) return;

		return $hblock;
	}

	protected function prepareEntity()
	{
		if(!Loader::includeModule("highloadblock"))return;

		$hblock = $this->getIblock();
		if(!$hblock) return;

		# преобразовать ХЛ-блок в сущность
		$entity = HiloadTable::compileEntity($hblock);
		$this->entityClass = $entity->getDataClass();
	}

	protected function prepareResult()
	{
		if(!empty($this->result)) return;
		$entity = $this->entityClass;

		// d($this->params,"params");
		// $this->result = $entity::getList($this->params);


		$main_query = new Entity\Query($entity);
		$main_query
			->setSelect($this->params["select"])
			->setFilter($this->params["filter"])
			->setOrder($this->params["order"])
			;

		if(isset($this->params["limit"])) {
			$main_query->setLimit($this->params["limit"]);
			$main_query->setOffset($this->params["offset"]);
		}


		$this->result = $main_query->exec();
		$this->result = new CDBResult($this->result);
	}

	public function setParams($params=[])
	{
		$this->params = array_merge($this->params,$params);
	}

	public function getNavString($count,&$data=false)
	{
		$iNumPage = is_set($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1;
		$this->params = array_merge([
			"limit" => $count,
			"offset" => ($iNumPage-1) * $count
		],$this->params);


		// $this->prepareResult();
		$entity = $this->entityClass;
		$data = new CDBResult($entity::getList([
			"select" => ["ID"],
			"filter" => $this->params["filter"]
		]));
		$data->NavStart($count);


		return $data->GetPageNavStringEx();
	}

	public function getList($params)
	{
		$array = [];
		$this->prepareResult();
		while ($item = $this->result->fetch()) {
			$array[] = $item;
		}
		return $array;
	}

}