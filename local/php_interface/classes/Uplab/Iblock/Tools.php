<?
namespace Uplab\Iblock;

use Uplab\Helper;
use Uplab\Constant;
use CIBlockProperty;
use CUserOptions;
use CConst;

/**
* Helper class with tools for iblocks
*/
class Tools
{
	protected $iblock = null;
	protected $iblockCode = null;
	protected $settings = null;

	function __construct($iblockCode,$lang=false)
	{
		if(!\Bitrix\Main\Loader::includeModule('iblock'))return;;
		if(!$lang)$lang = LANGUAGE_ID;
		$this->iblockCode = $iblockCode;
		$this->iblock = Constant::Get($lang,$iblockCode);
	}

	function printSettings($settings)
	{
		$arSettings = $this->makeFormArray($settings);
		echo '<pre>';
		var_export($arSettings);
		echo $this->makeFormString($arSettings);
		echo '</pre>';
	}

	function makeFormString($arSettings)
	{
		$settings = '';
		$i=1;
		foreach ($arSettings as $tabName => $arTab) {
			$settings .= 'uptab'.$i++.'--#--'.$tabName;
			foreach ($arTab as $code => $name) {
				$settings .= '--,--'.$code.'--#--'.$name;
			}
			$settings .= '--;--';
		}
		return $settings;
	}

	function makeFormArray($settings)
	{
		$arTabs = explode('--;--', $settings);
		$arSettings = array();
		foreach ($arTabs as &$tab) {
			$tab = explode('--,--', $tab);
			if(empty($tab[0]))continue;
			foreach ($tab as $key => $field) {
				$field = explode('--#--',$field);
				if($key===0){
					$tabName = $field[1];
					continue;
				}
				$arSettings[$tabName][$field[0]] = $field[1];
			}
		}
		return $arSettings;
	}

	function getProps()
	{
		$arProps = array();
		$res = CIBlockProperty::GetList(
			array('SORT' => 'ASC'),
			array('IBLOCK_ID' => $this->iblock, 'CHECK_PERMISSIONS' => 'N')
		);
		while($prop = $res->Fetch()){
			$arProps[$prop['ID']] = $prop['CODE'];
		}
		return $arProps;
	}

	public function exportFormSettings($dir=false)
	{
		if(!$dir)$dir=__DIR__;
		$path = $dir.'/export_form_'.strtolower($this->iblockCode).'_'.date('Ymdhis').'.php';
		$settings = $this->getFormSettings();
		echo $path;
		file_put_contents($path, $settings);
		return $path;
	}

	public function getFormSettings()
	{
		$settings = CUserOptions::GetOption('form', 'form_element_'.$this->iblock, true)['tabs'];
		$arProps = $this->getProps();
		foreach ($arProps as $key => $code) {
			$settings = str_replace("--PROPERTY_$key--", "--PROPERTY_%$code%--", $settings);
		}
		// $this->printSettings($settings);
		$settings =
			'<?return '.
			var_export($this->makeFormArray($settings),1).
			';?>';
		return $settings;
	}

	public function importFormSettings($path,$settings=false)
	{
		if(!$settings){
			$settings = include($path);
			if(!$settings)return;
		}
		$settings = $this->makeFormString($settings);
		$arProps = $this->getProps();
		foreach ($arProps as $key => $code) {
			$settings = str_replace("--PROPERTY_%$code%--", "--PROPERTY_$key--", $settings);
		}
		$this->printSettings($settings);
		$arOptions = array(array(
			'd' => 'Y',
			'c' => 'form',
			'n' => 'form_element_'.$this->iblock,
			'v' => array('tabs' => $settings)
		));
		CUserOptions::SetOptionsFromArray($arOptions);
	}
}