<?
namespace Uplab;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;
use CIBlockSection;
use CIBlockElement;
use CFile;
use CHTTP;
use CTimeZone;
use CLang;
use Cutil;
use ZipArchive;

/**
* Class for Bitrix events
*/
class Events
{
	public static function updateConst()
	{
		Constant::update(Constant::getInstance()->path);
	}

	public static function redirect404()
	{
		if (defined('ADMIN_SECTION') || !defined("ERROR_404"))return;
		global $APPLICATION;
		global $USER;
		$ref = '/'.implode('/',array_slice(explode('/',$_SERVER['HTTP_REFERER']),3));
		$curDir = $APPLICATION->GetCurDir();
		if($_SERVER['HTTP_REFERER'] && isset($_REQUEST['lang']))
		{
			$adr = explode('/',$curDir);
			array_pop($adr);
			array_pop($adr);
			LocalRedirect(implode('/',$adr).'/?lang');
		}
		else
		{
			$APPLICATION->RestartBuffer();
			include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/header.php";
			if(file_exists($_SERVER['DOCUMENT_ROOT'].SITE_DIR."404.php"))
				include $_SERVER['DOCUMENT_ROOT'].SITE_DIR."404.php";
			include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/footer.php";
		}
	}

	public static function updateVideo(&$arFieds)
	{
		$iblock = $arFieds["IBLOCK_ID"];
		$id = $arFieds["ID"];
		$el = new CIBlockElement;

		$updated = "updated_{$id}";
		if($GLOBALS[$updated])return;
		$GLOBALS[$updated] = true;


		// AddMessage2Log($arFieds);


		if(!in_array($iblock,
			[VIDEO_IBLOCK,PRESS_IBLOCK]
		))return;

		if(!Loader::includeModule("iblock"))return;


		$arFilter = array("IBLOCK_ID" => $iblock,"ID" => $id);
		$arSelect = array(
			"ID","IBLOCK_ID","NAME","CODE",
			"TAGS",
			"PREVIEW_PICTURE",
			"PREVIEW_TEXT",
			"PROPERTY_VIDEO_URL"
		);
		$res = CIblockElement::getList(
			$arOrder,$arFilter,false,
			array("nTopCount"=>1),
			$arSelect
		);
		if (!$arItem = $res->GetNext()) return;

		$video = new Video($arItem["PROPERTY_VIDEO_URL_VALUE"]);
		$videoData = $video->getVideoData();

		$arLoad = array();

		if (!$arItem["PREVIEW_PICTURE"])
			$arLoad["PREVIEW_PICTURE"] = CFile::MakeFileArray($videoData["picture"]);

		if ($arItem["NAME"]=="*" && $videoData["title"])
			$arLoad["NAME"] = $videoData["title"];

		if ($arItem["CODE"]=="*" && $videoData["code"])
			$arLoad["CODE"] = $videoData["code"];

		if (!$arItem["TAGS"] && $videoData["tags"])
			$arLoad["TAGS"] = $videoData["tags"];

		if (!$arItem["PREVIEW_TEXT"] && $videoData["description"]) {
			$arLoad["PREVIEW_TEXT"] = $videoData["description"];
			$arLoad["PREVIEW_TEXT_TYPE"] = "html";
		}

		if (!empty($arLoad))
			$res = $el->Update($id, $arLoad);

		CIBlockElement::SetPropertyValuesEx($id, $iblock, array(
			"VIDEO_URL" => array(
				"VALUE" => $arItem["PROPERTY_VIDEO_URL_VALUE"],
				"DESCRIPTION" => $videoData["time"]
			)
		));

		// AddMessage2Log(print_r([$arLoad,$videoData],1));

	}
}