<?
namespace Uplab;

/**
* class for defining and operating constants
*/
class Constant
{
	protected static $instance  = null;
	protected static $constData = "const_data.php";
	protected static $constFile = "constants.php";

	function __construct() {
		$path = realpath(__DIR__."/../../include"); //."/constants.php";
		if(!is_array($const=$this->readData($path)) || $const===true){
			$this->update($path);
			$const=$this->readData($path);
		}
		$this->const = &$const;
		$this->path = $path;
	}

	protected function readData($path) {
		return include $path.DIRECTORY_SEPARATOR.self::$constFile;
	}

	public static function getInstance() {
		if (is_null(self::$instance)){
			self::$instance = new static();
		}
		return self::$instance;
	}

	public static function define()
	{
		foreach (self::getArray(LANGUAGE_ID) as $key => $value){
			define($key, $value);
		}
	}

	public static function get($name, $lang=LANGUAGE_ID){
		return self::getArray($lang)[$name];
	}

	public static function getRow($name){
		$arRow = array();
		$arConsts = self::getAll();
		foreach($arConsts as $ln=>$arLang){
			if($arLang[$name])
				$arRow[$ln] = $arLang[$name];
		}
		return $arRow;
	}

	public static function getArray($lang){
		return self::getAll()[$lang];
	}

	public static function getAll(){
		return self::GetInstance()->const;
	}

	public static function getIblock($code){
		$const = constant(strtoupper($code) . "_IBLOCK");
		return intval($const) ? $const : false;
	}

	public static function extract($str,$params=array(),$clean=true){
		preg_match_all('/\#([a-zA-Z0-9_-]+)\#/',$str,$matches);
		foreach ($matches[0] as $key => $match) {
			$const = $matches[1][$key];
			if(isset($params[$const]))
				$replace = $params[$const];
			elseif(defined($const))
				$replace = constant($const);
			else
				continue;
			// $replace = "";
			$str = str_replace($match, $replace, $str);
		}
		if($clean)
			$str = preg_replace('/(\/+)/','/',$str);
		return $str;
	}

	public static function update($path=false){
		if(!\Bitrix\Main\Loader::includeModule("iblock"))return;

		// AddMessage2Log($path);

		if(!$path || is_array($path)){
			if(is_null(self::$instance))return;
			$path = self::$instance->path;
		}

		$arSites = include $path.DIRECTORY_SEPARATOR.self::$constData;

		$arResult = array();
		$res = \CIBlock::GetList();
		$len = 0;
		while($ib = $res->Fetch())
		{
			// AddMessage2Log($ib);
			if($ib["CODE"]){
				$code = strtoupper(str_replace("-", "_", $ib["CODE"]))."_IBLOCK";
				$len = strlen($code);
				if($len>$keyLen) $keyLen = $len;

				$len = strlen($ib["ID"]);
				if($len>$numLen) $numLen = $len;

				$arResult[$ib["IBLOCK_TYPE_ID"]]["ITEMS"][$ib["ID"]] = array(
					"NAME" => $ib["NAME"],
					"CODE" => $code,
					"ID"   => $ib["ID"]
				);
			}
		}

		$str = ""; $i=0; $cnt=count($arSites);
		$str.=		"<?php".PHP_EOL;
		$str.=		"return array(".PHP_EOL;

		foreach ($arSites as $site => $arSite) {
			$str.=	  "	'{$site}' => array(".PHP_EOL;

			$first=true;
			foreach ($arSite["types"] as $key => $type) {
				if(!$first)
					$str.=  PHP_EOL;
				$str.=	"		'{$key}' ".str_repeat(" ", $keyLen-strlen($key))."=> '$type',".PHP_EOL;
				// $str.=	PHP_EOL;

				$first=true;
				foreach ($arResult[$type]["ITEMS"] as $id => $iblock) {
					if($first)
						$str.=  PHP_EOL;
					$str.=  "		'{$iblock['CODE']}' ".str_repeat(" ", $keyLen-strlen($iblock['CODE']))."=> {$iblock['ID']}, ".str_repeat(" ", $numLen-strlen($iblock['ID']))."# {$iblock['NAME']}".PHP_EOL;
				$first=false;
				}

			$first=false;
			}

			$first=true;
			foreach ($arSite["add"] as $key => $value) {
				// if(!intval($value) || intval($value)!=$value)
				$value = "'$value'";
				if($first) $str.= PHP_EOL;
				$str.=	  "		'{$key}' ".str_repeat(" ", $keyLen-strlen($key))."=> {$value}, ".str_repeat(" ", $numLen-strlen($value)).PHP_EOL;

			$first=false;
			}

			$str.=		  "	)".($i<$cnt-1?',':'').PHP_EOL;
			$i++;
		}
		$str.=			  ");".PHP_EOL.PHP_EOL;
		$str.=			  "//This file automatically generated by ".str_replace($_SERVER["DOCUMENT_ROOT"], "", __FILE__)." at ".date("c").PHP_EOL;
		$str.=			  "?>";

		file_put_contents($path.DIRECTORY_SEPARATOR.self::$constFile, $str);

		return $str;
	}
}