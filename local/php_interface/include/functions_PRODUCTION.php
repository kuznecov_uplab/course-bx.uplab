<?
# debug function v25.11.16
function d($array='',$name='')
{
	return;
}


function df($array='',$name='')
{
	if(is_array($array)) $array['__keys__'] = array_keys($array);
	if(empty($array)) $array = (bool)$array;
	$array = Bitrix\Main\Web\Json::encode($array, JSON_HEX_TAG | JSON_UNESCAPED_UNICODE);
	echo '<script data-skip-moving=true>';
	if($name) {
		echo 'console.info("',$name,':",', $array, ');';
	} else {
		echo 'console.info(',$array,');';
	}
	echo '</script>',PHP_EOL;
}