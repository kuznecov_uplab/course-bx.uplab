<?
# debug function v11.12.16
function d($array='',$name='')
{
	if(is_array($array)) $array['__keys__'] = array_keys($array);
	if(empty($array)) $array = (bool)$array;
	$array = Bitrix\Main\Web\Json::encode($array, JSON_HEX_TAG | JSON_UNESCAPED_UNICODE);
	// $array = json_encode(unserialize((serialize($array))));
	echo '<script data-skip-moving=true>';
	if($name) {
		echo 'console.info("',$name,':",', $array, ');';
	} else {
		echo 'console.info(',$array,');';
	}
	echo '</script>',PHP_EOL;
}