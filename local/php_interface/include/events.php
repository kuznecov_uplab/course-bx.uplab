<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler(
	"iblock", "OnAfterIBlockAdd",
	array("Uplab\\Constant", "update")
);

EventManager::getInstance()->addEventHandler(
	"iblock", "OnAfterIBlockUpdate",
	array("Uplab\\Constant", "update")
);

EventManager::getInstance()->addEventHandler(
	"main", "OnEpilog",
	array("Uplab\\Events", "redirect404")
);