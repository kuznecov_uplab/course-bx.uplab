<aside class="col-md-4">
	<div class="laread-right">
		<form class="laread-form search-form">
			<div class="input"><input type="text" class="form-control" placeholder="Search..."></div>
			<button type="submit" class="btn btn-link"><i class="fa fa-search"></i></button>
		</form>
<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list","aside",
	array(
		"VIEW_MODE" => "TEXT",
		"SHOW_PARENT_NAME" => "Y",
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => ARTICLES_IBLOCK,
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => "",
		"SECTION_USER_FIELDS" => "",
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent("bitrix:news.list", "aside.recent", array(
	"IBLOCK_ID" => ARTICLES_IBLOCK,
	"SORT_BY1" => "sort",
	"SORT_ORDER1" => "asc",
	"SORT_BY2" => "date_active_from",
	"SORT_ORDER2" => "desc",
	"SET_TITLE" => "N",
	"NEWS_COUNT" => "5"
));?>
		<ul class="laread-list">
			<li class="title">TAGS</li>
			<li class="bar-tags">
				<a href="#">fashion</a>
				<a href="#">culture</a>
				<a href="#">art</a>
				<a href="#">concept</a>
				<a href="#">style</a>
				<a href="#">advert</a>
				<a href="#">movie</a>
				<a href="#">color</a>
				<a href="#">branding</a>
				<a href="#">technology</a>
				<a href="#">fashion</a>
				<a href="#">culture</a>
				<a href="#">art</a>
				<a href="#">concept</a>
			</li>
		</ul>
		<ul class="laread-list barbg-grey">
			<li class="title">NEWSLETTER</li>
			<li class="newsletter-bar">
				<p>Vivamus nec mauris pulvinar leo dignissim sollicitudin eleifend eget velit.</p>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" class="form-control" placeholder="john@doe.com">
					<span class="input-group-btn">
						<button class="btn" type="button"><i class="fa fa-check"></i></button>
					</span>
				</div>
			</li>
		</ul>
		<div class="laread-list quotes-basic">
			<i class="fa fa-quote-left"></i>
			<p>“The difference between stupidity and genius is that genius has its limits.”</p>
			<span class="whosay">- Albert Einstein </span>
		</div>
		<ul class="laread-list social-bar">
			<li class="title">FOLLOW US</li>
			<li class="social-icons">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-google-plus"></i></a>
				<a href="#"><i class="fa fa-dribbble"></i></a>
			</li>
		</ul>
	</div>
</aside>